﻿using System;
using System.Runtime.InteropServices;

namespace ComInteropClassLib
{
	[Guid("E3F0D789-D0D6-45E2-A117-70318DB4C709")]
	[ClassInterface(ClassInterfaceType.None)]
	public class Calculator : ICalculator
    {
		public int A { get; set; }
		public int B { get; set; }

		public Calculator()
		{
			A = 0;
			B = 0;
		}

		public int Sum(int c, int d)
		{
			return A + B + c + d;
		}
	}
}
