﻿using System.Runtime.InteropServices;

namespace ComInteropClassLib
{
	[Guid("E02FCCEA-09C1-45ED-90A0-852421A1A690")]
	public interface ICalculator
	{
		int A { get; set; }
		int B { get; set; }

		int Sum(int c, int d);
	}
}