# README.md

## Introduction

This is a project to register .NET dlls for COM Interop without admin rights using Powershell.

## Content

- **ComInteropClassLib**: a .NET class library written for COM interop.
- **Test-SetRegistryForNetDll.ps1**: script to register *ComInteropClassLib.dll*
- **Set-RegistryForNetDll.ps1**: Powershell script to register or unregister a dll.
- **Test-ComInteropClassLib.ps1**: Script to run the VBScript file *TestComInteropClassLib.vbs* with cscript x64 and cscript x86.
- **TestComInteropClassLib.vbs**: VBScript file to test the creation of COM object.

## How to use this project

1. Check the *ComInteropClassLib* VS solution code.
2. Build the solution.
3. Test that nothing works:
	```
	cscript .\TestComInteropClassLib.vbs
	```
4. Register *ComInteropClassLib.dll* (change the path if you're targeting x86 architecture):
	```
	.\Set-RegistryForNetDll.ps1 -Path .\ComInteropClassLib\ComInteropClassLib\bin\x64\Release\ComInteropClassLib.dll
	```
5. Test that everything works:
	```
	cscript .\TestComInteropClassLib.vbs
	```
6. Unregister *ComInteropClassLib.dll*:
	```
	.\Set-RegistryForNetDll.ps1 -Path .\ComInteropClassLib\ComInteropClassLib\bin\x64\Release\ComInteropClassLib.dll -Unregister
	```
7. Test that nothing works anymore:
	```
	cscript .\TestComInteropClassLib.vbs
	```
