################################################################################
# This scroll gives one the power to leave marks on both realms of the world
# without summoning the power of the gods.
#
# Translation:
# This script allows to register or unregister .NET dlls
# for COM Interop without admin rights.
# It registers the dll for both x86 and x64 architectures.
#
# Please note that:
#	o	although a x64 dll can be registered as a x86 dll,
#		it will not be possible to create an object on a x86 OS.
#		You will be greeted with an error.
#	o	Unregistering a dll, whether with or without the -X86 switch
#		will make the dll not work anymore for both architectures.
#	
################################################################################

param(
	[Parameter(Mandatory = $true)]
	[string]
	$Path,
	[switch]
	$Unregister
)

function Set-RegistryForNetDll {
	param (
		[string]
		$Path,
		[switch]
		$Unregister,
		[switch]
		$X86
	)

	$regasmX86 = 'C:\Windows\Microsoft.NET\Framework\v2.0.50727\RegAsm.exe'
	$regasmX64 = 'C:\Windows\Microsoft.NET\Framework64\v2.0.50727\RegAsm.exe'
	$regClsidX86 = 'Classes\Wow6432Node\CLSID'
	$regClsidX64 = 'Classes\CLSID'
	$regUserSoftwareClasses = 'HKEY_CURRENT_USER\Software\Classes'
	$fileBaseName = (Get-Item -Path $Path).BaseName

	if ($Unregister) {
		$regUserSoftwareClasses = "-$regUserSoftwareClasses"
		$regFileName = 'unregister'
	}
	else {
		$regFileName = 'register'
	}

	$regFileName += ".$fileBaseName"

	if ($X86) {
		$regasm = $regasmX86
		$regClsid = $regClsidX86
		$regFileName += '.X86'
	}
	else {
		$regasm = $regasmX64
		$regClsid = $regClsidX64
		$regFileName += '.X64'
	}

	$regFileName += '.reg'
	
	& $regasm $Path /codebase /regfile:$regFileName /nologo

	$regFileContent = Get-Content $regFileName
	$regFileContent = $regFileContent.Replace('HKEY_CLASSES_ROOT', $regUserSoftwareClasses)
	$regFileContent = $regFileContent.Replace('Classes\CLSID', $regClsid)
	Set-Content -Path $regFileName -Value $regFileContent

	reg.exe import $regFileName

	# If you want to see what the .reg files look like, comment this line
	Remove-Item $regFileName
}

if ($Unregister) {
	Set-RegistryForNetDll -Path $Path -Unregister
	Set-RegistryForNetDll -Path $Path -Unregister -X86
}
else {
	Set-RegistryForNetDll -Path $Path
	Set-RegistryForNetDll -Path $Path -X86
}
